export const environment = {
  production: true,
  //Llamamos a la ruta API que va a proveer Laravel
  base: 'http://127.0.0.1:8000/api/'
  //Básicamente es el nombre de la ruta principal
  //que nos va a permitir conectarnos con Laravel
};
