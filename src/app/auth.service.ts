import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  base = environment.base;

  constructor(private http: HttpClient) { }

  // Devuelve la petición http, un json que contiene a los usuarios con todos sus datos
  getUser() {
    var aux = this.base;

    return this.http.get(aux + 'users');

    // Otra forma de hacer lo mismo
    // return this.http.get('${this.base}users');
  }
}
