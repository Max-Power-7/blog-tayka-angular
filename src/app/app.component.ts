import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'blogTayka';

  // Contendrá en un array a los usuarios obtenidos por el request
  usuarios = [];

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    //Llama al método getUser el cual devolverá los datos y se asigarán al array usuarios
    this.authService.getUser().subscribe((data: any) => {
      // console.log(data);
      this.usuarios = data;
    });
  }
}
